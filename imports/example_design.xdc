#Pin LOC constraints for the status signals init_calib_complete and data_compare_error

#Ports related to the System

# Bank 66 - GPIO_LED0_LS
set_property PACKAGE_PIN AR19 [get_ports {led[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {led[0]}]
set_property SLEW SLOW [get_ports {led[0]}]

# Bank 66 - GPIO_LED1_LS
set_property PACKAGE_PIN AT17 [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS12 [get_ports {led[1]}]
set_property SLEW SLOW [get_ports {led[1]}]

# Bank 66 - GPIO_LED2_LS
set_property PACKAGE_PIN AR17 [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS12 [get_ports {led[2]}]
set_property SLEW SLOW [get_ports {led[2]}]

# Bank 66 - GPIO_LED3_LS
set_property PACKAGE_PIN AU19 [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS12 [get_ports {led[3]}]
set_property SLEW SLOW [get_ports {led[2]}]

# Bank 84 CPU_RESET
set_property PACKAGE_PIN AU1 [get_ports {sys_rst}]
set_property IOSTANDARD LVCMOS12 [get_ports {sys_rst}]
set_property SLEW SLOW [get_ports {sys_rst}]
