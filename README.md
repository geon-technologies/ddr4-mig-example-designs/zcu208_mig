# zcu208-ddr4-mig

## Getting started

Xilinx provides DDR4 Mig Core tutorials for both the zcu102 and zcu111. However, it does not
provide a tutroial for the zcu208. Work has been done to understand what was done during the
zcu102 and zcu111 tutorials and apply that knowledge to the zcu208 in order to implement the
same behavior on the zcu208. Given that the zcu102 and zcu111 tutorials are very similar, the
user is encouraged to reference either of those tutorials when implementing this repo.

Here is the link to the tutorial, navigate to the 'XTP432 - ZCU102 Mig Tutorial (v11.0)':
https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html

Here is the link to the tutorial, navigate to the 'XTP514 - ZCU111 Mig Tutorial (v2.0)':
https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html

Be sure to install the PDF and the associated zip file.

If the link has been broken or no longer exists, provided in this repo under docs is the provided
pdf and the accociated zip file.

## Setup the Hardware

1. Setup the ZCU 208 and the JTAG Pod to the board.

2. Follow this link if you need help setting up JTAG Drivers:

   https://digilent.com/reference/programmable-logic/guides/install-cable-drivers

   $ cd [Vivado Install]/data/xicom/cable\_drivers/lin64/install\_script/install\_drivers/

   $ ./install\_drivers

2. Be sure that the 'Mode Switch SW2 COnfiguration Option Seeting'  are in JTAG mode:

   Mode Pins[3:0] 0000

## Building the project

3. Source vivado 2021.2

4. Tools > Run Tcl Script.. > Select the ddr4\_0\_ex.tcl

5. Generate Bitstream

## Test On Hardware

1. Turn on the ZCU 208

2. With the bitstream built -> Open hardware Manager -> Open Target -> Auto Connect

3. Program Device -> Should have auto-populated the example\_top.bit bitstream file

4. Visually confirm that the LEDs on the board are behaving as described in the MIG tutorial PDF

5. In the "Hardware" window in vivado navigate to localhost(1) -> xilinx\_tcf -> xcvu28dr\_1 (4)

6. Double click the hw\_vio\_1 (u\_led\_display\_driver)

7. Press the + button to add probes -> select the u\_led\_display\_driver

8. Expand the u\_led\_display\_driver to see all 4 LEDs

9. Confirm that the LED vio's are behaving as described in the MIG tutorial PDF
